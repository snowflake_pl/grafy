/*
 * funkcje.cpp
 *
 *  Created on: 21-11-2013
 *      Author: sniegu
 */

#include <iostream>
#include <math.h>
#include <cstdlib>
#include <iomanip>
#include <time.h>
#include "clasy.h"
using namespace std;

void sprawdz(int ** A, int a, int n, bool * odwiedzone)//sprawdza sciezki z punku a do wszystkich
{
	odwiedzone[a] = true;					//zaznacz że a-ty odwiedzony
	for (int i=0;i<n;i++)					//wszystkie mozliwe krawedzie z a-tego
	{
		if ((i==a) || (odwiedzone[i]))		//pominiecie petli i juz odwiedoznych
			continue;
		if (A[a][i])						//istnieje krawedz do i-tego
		{
			odwiedzone[i]=true;				//zaznacz ze i-ty odwiedzony
			sprawdz(A,i,n,odwiedzone);		//sprawdz zaczynajac w i-tym
		}
	}
}

void stopniuj(int **A, int n, int * stopnie)//liczy stopnie wierzcholkow
{
	for (int i=0;i<n;i++)	//obliczamy wagi wierzcholkow
	{
		stopnie[i]=0;		//zero na start

		for (int j=0;j<n;j++)
		{
			if (j==i)		//pomijamy diagonale
				j++;

			if (A[i][j])	//istnieje krawedz z i->j
				stopnie[i]++;//zwieksz stopien wierzcholka i
		}
	}
}

void usun_krawedz(int **A,int n)// usuwa krawedz zeby losowac nowa
{
	int * stopnie = new int[n];
	stopniuj(A,n,stopnie);			//liczymy stopnie

	int start  = rand() % n;		//losujemy start usuwanej krawedzi
	int koniec = rand() % n;		//losujemy koniec usuwanej krawedzi

	while (stopnie[start]==0)		//ze startu nie ma krawedzi
		start = rand() % n;			//losuj nowy wierzcholek startowy

	//w tym momencie start jest ustawiony na wierzcholek z jakas krawedzia

	while (!A[start][koniec])		//nie ma krawedzi start->koniec
		koniec = rand() % n;			//losuj nowy koniec

	//w tym momencie mamy wybrana istniejaca krawedz

	A[start][koniec] = 0;			//usunelismy ja

	delete[] stopnie;				//czyscimy pamiec po sobie
	return;							//wychodzimy
}

bool spojnosc(int **A,int n)//sprawedza spojnosc calego grafu
{
	bool * odwiedzone = new bool[n];
	bool spojny = true;								//spojnosc calego grafu

	for (int j=0;j<n;j++)						//sprawdzamy z kazdego wierzcholka
		{
			for (int i=0;i<n;i++)
				odwiedzone[i]=false;			//wszystkie wierzcholki sa nieodwiedzone

			sprawdz(A,j,n,odwiedzone);			//sprawdzamy z j-tego wierzcholka

			for (int i=0;i<n;i++)				//sprawdzamy czy wszystkie odwiedzone
				if (odwiedzone[i]==false)		//jesli nie
				{
					delete[] odwiedzone;
					return (spojny=false);		//zapisz i zwroc false
				}
		}
	delete[]  odwiedzone;
	return spojny;								//zwroc stan spojnosci (true)
}

bool losuj(int ** A,int n)//losuje nowa krawedz
{
	int a = rand() % n;				//losowanie startu
	int b;							//zmienna konca
	while (a==(b = rand() % n)) ;	//losowanie konca roznego od startu
	int w = 1+ (rand() % 10);		//losowanie wagi z zakresu 1-100

	if (A[a][b])					//wylosowana krawedz juz jest
		return false;				//zwroc false - brak nowej

	else
	{
		A[a][b] = w;
		return true;				//przypisz nowa wage
	}	//zwracamy wage niezerowa, wiec true
}

void print(int ** A, int n)	//drukuje tablice na ekran
{
	cout <<endl;
	for (int i = 0;i<n;i++)
	{
		for (int j=0;j<n;j++)
			cout <<setw(6)<<A[i][j]; //printowanie o stalej szerokosci

		cout <<endl;
	}
}


void generuj(int **A, int n)
{
	int g;
	cout <<"Podaj procent zapełnienia: ";
	cin >>g;

	int min = n-1; 			//minimalna liczba krawedzi dla nieskierowanego
	int maxS=n*n-n;			//maksymalna liczba krawedzi skierowanego
	int maxN = maxS/2;		//maksymalna liczba krawedzi nieskierowanego
	g = (g>100 ? 100 : g);	//maksymalnie 100%
	int ms = g*maxS/100;	//liczba krawedzi z gestosci dla skierowanego
	int mn= g*maxN/100;		//liczba krawedzi z gestosci dla nieskierowanego

	mn = (mn< min ? min: mn); 			//jesli mniej niz min - przypisz min, jesli nie - zostaw
	ms = (ms< n ? n: ms); 				//jesli mniej niz min = n - przypisz min, jesli nie - zostaw

	int start, koniec, waga, root;

	int * used = new int[n+1];
	root = koniec = rand()% n; 			//losujemy koniec krawedzi
	used[0] = koniec;
	for (int i=0;i<n-1;i++)
	{
		bool usedalready = false; 		//wylosowano nieuzywany
		do
		{
			usedalready = false;
			start = rand()% n;			//losowanie
			for (int j=0;j<=i;j++)		//sprawdzanie czy nowy
				if (start == used[j]) 	//nie nowy
				{
					usedalready = true;	//zaznacz
					break;				//przerwij for
				}
		} while(usedalready); 			//wylosowany nowy unikalny start

		waga = 1+rand()%10;  			//losowa waga
		used[i+1] = start;				//zapisanie ze start juz uzyty
		A[start][koniec] = waga;		//zapisanie wagi
		koniec = start;					//start  staje sie koncem kolejnej krawedzi (skier)
//		koniec = used[rand()%(i+1)]; 	//losowanie sposrod juz uzytych dla nowego konca //graf nieskierowany
	}

	waga =  1+rand()%10;				//losowanie wagi
	A[root][start] = waga;				//zamkniecie cyklu hamiltona

	int z = ms - n;						//pozostala ilosc do wylosowania (ms-n // mn - n-1 (NSKIER))
	while(z--)			//zmniejsz pozostala ilosc
		while (!losuj(A,n));    //losuj nowa krawedz
}


int ** Prim(int ** A, int n)
{
	int ** MST = new int*[n];	//nowa tablica
	for (int k=0;k<n;k++)
	{							//dwuwymiarowa
		MST[k] = new int[n];//na drzewo rozpinające
		for (int i=0;i<n;i++)
			MST[k][i]=0;
	}

//	int i=0;					//zmienna na id
	lista list;					//lista krawedzi
	list.AddVert(A[0],n,0,true);		//dodajemy wszystkie krawedzie z weirzcholka 0
	cout <<"dodaje wierzcholek 0"<<endl;							//beda posortowane wzgledem wag

	while (list.vertcount<n)	//dopoki nie dodamy wszystkich wierzcholkow grafu
	{
		list.Sort();
		int id = list[0].id;	//zapisujemy ID pierwszej krawedzi na liscie

		//list[0].end = koniec najtanszej krawedzi z listy
		if (list.VertInTree(list[0].end))//koniec krawedzi jest juz w drzewie
		{
			list.Del(id);		//skasuj ja z listy
			//continue;			//przejdz dalej
		}
		else					//koniec krawedzi nie jest dodany do drzewa
		{
			cout <<"dodaje wierzcholek "<<list[0].end<<endl;
			MST[list[0].end][list[0].start] = MST[list[0].start][list[0].end] = list[0].waga;	//wpisanie wagi krawedzi do tablicy z drzewem
			list.AddVert(A[list[0].end],n,list[0].end,true);		//dodaje wierzcholek do listy dodanych do drzewa
			list.Del(id);											//kasujemy krawedz z listy
		}
	}

	return MST;					//zwracamy adres tablicy z MST
}

int ** kruskal(int ** A, int n)
{
	int ** MST = new int*[n];	//nowa tablica
	for (int k=0;k<n;k++)
	{							//dwuwymiarowa
		MST[k] = new int[n];	//na drzewo rozpinające
		for (int i=0;i<n;i++)
			MST[k][i]=0;
	}

	int *kolory = new int[n];	//lista wierzcholkow - kolorów drzew
	for (int i=0;i<n;i++)
		kolory[i] = i;			//kazdy ma swoj nr i przynaleznosc
									//do jednego z n drzew

	lista list;

	for (int i=0;i<n;i++)
		list.AddVert(A[i],n,i,false);	//dodajemy wszystkie krawedzie do listy

	list.Sort();						//sortujemy po wagach
//	int vertsAdded=0;
	while (list.count >0)				//dopoki lista krawedzi niepusta
	{
		int st = list[0].start;			//pobranie startu i konca krawedzi najtaszej
		int end = list[0].end;			//j.w.

		if (kolory[st] != kolory[end])		//konce krawedzi naleza do innych drzew
		{
			int staretree = kolory[end];	//zapisanie starego id drzewa
			int nowetree = kolory[st];		//zapisanie nowego id drzewa
			for (int i=0;i<n;i++)						//sprawdzamy wszystkie wierzcholki
				if (kolory[i] == staretree)		//jesli i-ty nalezal do starego drzewa
					kolory[i] = nowetree;			//zmieniamy mu id na id nowego drzewa

			MST[st][end] = MST[end][st] = list[0].waga;	//dodajemy do macierzy drzewa spinajacego nowa krawedz
			list.Del(list[0].id);						//kasujemy krawedz z listy
			cout <<"dodaje krawedz "<<st<<"->"<<end<<endl;

			int vertsAdded=0;					//liczba dodanych wierzcholkow do drzewa
			for (int i=0;i<n;i++)				//petla po wszystkich
				if (kolory[i]==nowetree)		//kolor itego to kolor drzewa do ktorego dodawalem krawedz
					vertsAdded++;				//zwieksz liczbe dodanych do drzewa

			if (vertsAdded==n)					//dodano wszysktie wierzcholki grafu do drzewa
				return MST;						//wyjdz z algorytmu
		}
		else 									//wierzcholki w tym samym drzewie
			list.Del(list[0].id);				//tylko kasujemy krawedz z listy
	}
	return MST;

}
int waga(int ** A, int n);
bool comaremst(int ** A, int ** B, int n)
{

	if (waga(A,n) - waga(B,n))
		return false;

	return true;
}

int waga(int ** A, int n)
{
	int weight=0;
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++)
			weight+=A[i][j];

	return weight/2;
}
