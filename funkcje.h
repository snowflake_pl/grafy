/*
 * funkcje.h
 *
 *  Created on: 21-11-2013
 *      Author: sniegu
 */

#ifndef FUNKCJE_H_
#define FUNKCJE_H_


void sprawdz(int ** A, int a, int n, bool * odwiedzone);
void stopniuj(int **A, int n, int * stopnie);
void usun_krawedz(int **A,int n);
bool spojnosc(int **A,int n);
bool losuj(int ** A,int n);
void print(int ** A, int n);
void generuj(int **A, int n);
int waga(int ** A, int n);
bool comaremst(int ** A, int ** B, int n);
int ** Prim(int ** A, int n);
int ** kruskal(int ** A, int n);

//void MatToElist(int **A,int n, lista& list);


#endif /* FUNKCJE_H_ */
