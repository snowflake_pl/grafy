#include <iostream>
#include <math.h>
#include <cstdlib>
#include <iomanip>
#include <time.h>
#include "funkcje.h"
#include "clasy.h"
#include <fstream>
#include <vector>
#include <list>
using namespace std;
const int INF = 999999;		//stała nieskonczonosciowa

bool Qempty(bool* q,int n)
{
	for (int i=0;i<n;i++)
		if (q[i])
			return false;

	return true;
}

int ** dijkstra(int **A, int n,int s) //s = start
{
	//int i,j,u,m,x,y,z,v0;

	int 	* d = new int[n],		//tablica kosztów przejscia - uzycie: d[i] = koszt przejscia do i (od s - parametr wywolania)
			* p = new int[n];		//lista poprzedników na najtanszej sciezce
	bool 	* Q = new bool[n],
			* S = new bool[n];


	// Inicjujemy struktury danych

	for(int i=0; i<n; i++)
	{
		d[i]  = (A[s][i] ? A[s][i] : INF);		// 	koszty dojścia (nieskonczonosc jesli brak sciezki)
		p[i]  = -1;     						// 	poprzednik na ścieżce
		Q[i] = true;							//	poczatkowo wszystkie w Q
		S[i] = false; 							//	poczotkowo zaden w S
	}

	d[s] = 0;									// 	droga do samego siebie jest darmowa


	while(!Qempty(Q,n))
	{
		int min = INF;

		int u=-1;
		for (int i=0;i<n;i++)	//szukamy elementu Q o najtanszym koszcie dojscia
			if (Q[i])			//ity jest w Q
			{
				if (min > d[i])
				{
					min = d[i];
					u = i;
//					cout <<"u = i = "<<i<<endl;
				}
			}
		cout <<"najtanszy sasiad: "<<u<<endl;
		Q[u] = false;		//	usuwamy najtanszy z Q
		S[u] = true;		//	dodajemy najtanszy do S

		for (int v=0;v<n;v++)
			if (A[u][v])	//istnieje krawedz u->v
			{
				cout <<"Sprawdzam do "<<v<<endl;
				if (d[v] > d[u] + A[u][v])			//taniej jest przez u niz do tej pory
				{
					cout <<"d["<<v<<"] >"<<"d["<<u<<"] + kraw("<<u<<","<<v<<")" <<endl;
					d[v] = d[u] + A[u][v];			//nowy koszt to ten przez u
					p[v] = u;						//poprzednik v to u (dla trasy)
					cout <<"dokonuje relaksacji..."<<endl;
				}
				else
				{
					cout <<"d["<<v<<"] <"<<"d["<<u<<"] + kraw("<<u<<","<<v<<")" <<endl;
					cout <<"Relaksacja zbędna"<<endl;
				}
			}
	}
	int ** result = new int*[2];
	result[0] = d;	//zapisujemy d i p i je zwracamy
	result[1] = p;
	return result;
}

bool relax(int * d, vector<Edge>& list)	//	relaxacja wszystkich krawedzi
{
	bool result =false;
	for (int i=0;i<list.size();i++)
	{
		int v = list[i].end;		//	koniec krawedzi
		int u = list[i].start;		//	poczatek krawedzi
		int C = list[i].waga;		//	waga krawedzi
		cout <<"Krawędź: |"<<u<<"|---"<<C<<"--->|"<<v<<"|"<<endl;
		cout <<"d["<<v<<"] = "<<d[v]<<endl;
		cout <<"d["<<u<<"] = "<<d[u]<<endl;


		if (d[v] > d[u] + C)		//	koszt bezposredni drozszy niz przez u
		{
			cout <<d[v]<<">"<<d[u]+C<<endl;
			cout <<"Dokonuje relaksacji..."<<endl;
			d[v] = d[u] + C;		// 	relaksuj krawedz
			result= true;			//	zwroc ze zrelaxowano (do sprawdzania ujemnych cykli)
		}
		else
			cout <<d[v]<<"<"<<d[u]+C<<endl<<"Relaksacja zbędna"<<endl;
	}
	return result;					//	nie zrelaxowano nic (juz git)
}
//int * bellford(int **A, int n,int s)
//{
//	int * d = new int[n];	//	tablica kosztów przejscia - uzycie: d[i] = koszt przejscia do i (od s - parametr wywolania)
//	lista list;									//	lista wszystkich krawedzi grafu
//
//	for(int i=0; i<n; i++)
//	{
//		d[i]  = INF;	//(A[s][i] ? A[s][i] : INF);		// 	koszty dojścia (nieskonczonosc jesli brak sciezki)
//		list.AddVert(A[i],n,i,false);			//	tworzymy liste wszystkich krawedzi
//	}
//	d[s] = 0;									// 	droga do samego siebie jest darmowa
//	for (int i=0;i<n-1;i++)						//	n-1 razy
//	{
//		cout <<endl<<"Przebieg nr :"<<i+1<<endl;
//		cout <<"Aktualna tablica kosztów: "<<endl;
//		for (int j=0;j<n;j++)
//			cout <<"d["<<j<<"]:"<<d[j]<<"\t";
//		cout <<endl;
//		relax(d,list);							//	robimy relaksacje
//	}
//	return d;									//	zwracamy wektor kosztow drog
//}
int * bellford(int **A, int n,int s)
{
	int * d = new int[n];	//	tablica kosztów przejscia - uzycie: d[i] = koszt przejscia do i (od s - parametr wywolania)
	vector<Edge> list;									//	lista wszystkich krawedzi grafu

	for(int i=0; i<n; i++)
	{
		d[i]  = INF;							// 	koszty dojścia = nieskonczonosc
		for (int j=0;j<n;j++)
			if ((A[i][j] < 1111) && (A[i][j] >0))
			{
				Edge * nowa = new Edge(i,j,A[i][j],0);
				list.push_back(*nowa);
				delete nowa;
				nowa = 0;
			}
	}
	d[s] = 0;
	int count;
	for (int i=0;i<n-1;i++)						//	n-1 razy
	{
		count=i;
		cout <<endl<<"Przebieg nr :"<<i+1<<endl;
		cout <<"Aktualna tablica kosztów: "<<endl;
		for (int j=0;j<n;j++)
			cout <<"d["<<j<<"]:"<<d[j]<<"\t";
		cout <<endl;
		if (!relax(d,list))
			break;							//	robimy relaksacje
	}

	if (count==n-2)							//zakonczylismy poprzez n-1 obiegów
		if (relax(d,list))					//można dokonac kolejnej relaksacji
			return 0;						//zwróc 0 bo cykl ujemny

	return d;									//	zwracamy wektor kosztow drog
}



int main()
{
	srand(time(0));
	int n;
	//time_t start, koniec;

	cout <<"Podaj ilosc wierzcholkow: ";
	cin >>n;


	int ** A = new int*[n];	//nowa tablica
	for (int i=0;i<n;i++)	//petla po wierszach
		A[i]=new int[n];	//tworzymy macierz sasiedztwa

	for (int i = 0;i<n;i++)
		for (int j=0;j<n;j++)
			A[i][j] = 0;	//zerowanie tablicy wag

	generuj(A,n);//losowanie grafu nieskierowanego

	print(A,n);

	for (int j=0;j<n;j++)
	{
		cout <<endl <<"startując z "<<j<<endl;
		int ** res = dijkstra(A,n,j);
		int * bf = bellford(A,n,j);
		cout <<"drogi dijkstrą: "<<endl;
		for (int i=0;i<n;i++)
			cout <<res[0][i]<<"\t";
		cout <<endl;
		if (!bf)
			cout <<"Graf zawierał cykl ujemny"<<endl;
		else
		{
			cout <<"drogi bellman ford: "<<endl;
			for (int i=0;i<n;i++)
				cout <<bf[i]<<"\t";
			cout <<endl;
		}
	}

	return 0;

}
