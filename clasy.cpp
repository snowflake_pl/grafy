/*
 * clasy.cpp
 *
 *  Created on: 22-11-2013
 *      Author: sniegu
 */


#include "clasy.h"

void lista::Add(Edge& toAdd)		//dodawanie krawedzi do listy
{
	//cout <<"Dodaje: "<<toAdd.start<<"->"<<toAdd.end<<endl;

	if (count==0)					//dodajemy pierwsza krawedz
	{
		//cout <<"Pierwsza krawedz!"<<endl;
		list = new Edge[1];
		toAdd.id =1;
		list[0] = toAdd;
		count++;
		return;
	}

	if (count > 0)					//dodajemy kolejna
	{

		//cout <<count+1<<"-ta krawedz!"<<endl;

		Edge * nowa = new Edge[count+1];

		if (list)
			for (int i=0;i<count;i++)
				nowa[i] = list[i];

		toAdd.id = count+1;

		nowa[count] = toAdd;
		count++;

		delete[] list;
		list = nowa;
		nowa=0;
	}

//	this->Sort();
}

void lista::Del(Edge& toDel)
{
	if (count==0)					//lista pusta
		return;

	if (list==0)					//lista pusta
		return;

	int found=-1;					//indeks kasowanego

	for (int i=0;i<count;i++)		//szukanie indeksu kasowanego elementu
		if (toDel.id==list[i].id)	//zgadza sie z rzadanym
		{
			found =i;				//zapisz indeks
			break;					//przerwij for
		}

	int j=0;						//indeks kopiowanych
	Edge * nowa = new Edge[count-1];//nowa tablica
	for(int i=0;i<count;i++)		//petla po wszystkich elementach zrodla
		if (i!=found)				//element nie ma kasowanego indeksu
			nowa[j++] = list[i];	//kopiuj go (pominie kasowany)

	count--;						//zmniejsz licznosc
	delete[] list;					//skasuj stara tablice
	list = nowa;					//zapisz adres nowej
	nowa=0;							//wyzeruj wskaznik
}

void lista::Del(int id)
{
	this->Del(this->EdgeById(id));
}


Edge& lista::EdgeById(int id)
{
	for (int i=0;i<count;i++)
		if (list[i].id == id)
			return list[i];

	return this->err;
}

int compare (const void * a, const void * b)
{
	Edge * ab, *ac;
	ab = (Edge*)a;
	ac = (Edge*)b;

	return ab->waga-ac->waga;
}
void lista::Sort()
{
	if (count==0)
		return;

	if (list==0)
		return;

	qsort(list,count,sizeof(Edge),compare);
}

void lista::AddVert(int * vector, int n, int start,bool check = true)
{	//dodaje wszystkie krawedzie wychodzace z wierzcholka start
	//w primie tylko te ktore nie koncza sie na wierzcholku juz dodanym do drzewa
	//w kruskalu wszysktie jakie istnieja



	if (check)							//sprawdzamy czy juz na liscie
		if (this->VertInTree(start))	//jesli wierzcholek juz dodany do drzewa
			return;						//wyjdz

	//cout <<"dodaje wierzcholek "<<start<<endl;

	//wierzcholek nie byl w drzewie wiec kontynuujemy





	AddVtoVec(start);					//dodaje wierzhcolek do listy dodanych do drzewa






	//cout <<endl<<endl<<"stan verts: "<<endl<<endl;
//	for (int i=0;i<vertcount;i++)
		//cout <<verts[i]<<endl;
	//cout <<endl<<endl;

	Edge * nowa =0;			//zmienna na tworzenie krawedzi

	for (int i=0;i<n;i++)	//przegladanie wszystkich krawedzi z danego wierzcholka
		if (vector[i])
		{
			//jesli jest krawedz do i-tego
			if (check)			//sprawdzamy czy koniec juz w drzewie - Prim
			{
				if (!this->VertInTree(i))		//konczy sie na wierzcholku spoza drzewa
				{
					//cout <<start<<"--"<<vector[i]<<"-->"<<i<<endl;
					nowa = new Edge(start, i, vector[i],0);	//utwórz obiekt krawedzi
					this->Add(*nowa);						//dodaj ją do listy krawedzi
					nowa=0;									//zeruj wskaznik
				}
			}
			else				//kruskal -- dodajemy zawsze
			{
				//cout <<start<<"--"<<vector[i]<<"-->"<<i<<endl;
				nowa = new Edge(start, i, vector[i],0);
				this->Add(*nowa);
				nowa=0;
			}
		}
}

bool lista::VertInTree(int toCheck)			//sprawdza czy wierzcholek juz w dzrzewie
{
	if (vertcount ==0 )		//	brak krawedzi, brak wierzcholkow
		return false;

	for (int i=0;i<vertcount;i++)		//przegladanie wszysktich dodanych wierzcholkow
		if (this->verts[i] == toCheck)	//sprawdzany juz dodany
			return true;				//zwroc prawde

	return false;						//dodajemy nowy wiec falsh
}


Edge lista::operator[](int i)			//umozliwia indeksowanie listy wewnetrznej z zewnatrz
{										//lista krawedzie (krawedzie.list[i] = krawedze[i])
	if ((i >= this->count)  || (i<0))
		return this->err;

	return this->list[i];
}

void lista::print()						//drukuje liste krawedzi na ekran
{
	for (int i = 0; i<count;i++)
		cout <<(this->list[i].start)<<"-"<<(this->list[i].waga)<<"->"<<(this->list[i].end)<<endl;
}

void lista::AddVtoVec(int start)		//dodaje wierzcholek do listy wierzcholkow juz w drzewie
{
	if (this->VertInTree(start))
		return;

	if (vertcount==0)
	{
		verts = new int[1];
		vertcount=1;
		verts[0]=start;
		return;
	}

	if (vertcount>0)
	{
		int * newa = new int[vertcount+1];
		for (int i=0;i<vertcount;i++)
			newa[i]=verts[i];

		newa[vertcount] = start;
		vertcount++;

		delete[] verts;
		verts = newa;
		newa =0;
	}
}
