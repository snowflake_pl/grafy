/*
 * clasy.h
 *
 *  Created on: 22-11-2013
 *      Author: sniegu
 */

#ifndef CLASY_H_
#define CLASY_H_

#include <cstdlib>
#include <iostream>
using namespace std;
class Edge					//klasa krawedzi
{
public:

	int start;				//poczatek krawedzi
	int end;				//koniec krawedzi
	int waga;				//waga krawedzi
	int id;					//id - do usuwania z listy
	Edge() : start(0),end(0),waga(0),id(0) {}
	Edge( int a, int b, int w, int id=0) :  start(a),end(b),waga(w),id(id) {}
};



class lista						//klasa listy krawedzi
{
	Edge err;					//element błędny - zwracany przy bledy
public:
	int count;					//liczba krawedzi na liscie
	Edge * list;				//tablica z krawedziami
	void Add(Edge& toAdd);		//dodaje krawedz do listy
	void Del(Edge& toDel);		//kasuje z listy
	void Del(int id);			//kasuje krawedz po id
	Edge& EdgeById(int id);		//zwraca referencje na krawedz o podanym id
	void Sort();				//sortuje liste po wagach
	void AddVert(int * vector, int n, int start, bool check);	//dodaje wszystkie krawedzie z wierzcholka start (check = false - nie sprawdza czy juz na liscie)
	bool VertInTree(int i);		//sprawdza czy wierzcholek juz na liscie dodanych
	Edge operator[](int i);		//indeksowanie listy
	lista(): err(-1,-1,-1,-1), count(0), list(0), vertcount(0), verts(0)  {}
	~lista() { if (count) if (list) delete [] list; list=0;}
	void print();				//wyswielta liste krawedzi
	void AddVtoVec(int start);	//dodaje wierzcholek do listy dodanych

	int vertcount;				//ilosc dodanych wierzcholkow
	int * verts;				//tablica dodanych wierzhcolkow

};

class Vert
{
public:
	int n; //nr wierszcholka
	int l; //nr drzewa
	Vert() : n(-1), l(-1) {}
	Vert(int n, int l=0) : n(n), l(l) {}
	void changetree(int l) { this->l = l; }


};

class node
{
public:
	int nr;
	int * nastepniki;
	int * poprzedniki;

	node(int n=0): nr(n), nastepniki(0), poprzedniki(0){}
	bool operator==(node& tocomare) { if (this->nr == tocomare.nr) return true; return false; }
	bool operator!=(node& tocomare) { return !(this->operator ==(tocomare)); }
};


class nodelist
{
public:
	node * wezly;
	int count;
	node null;
	nodelist():wezly(0),count(0), null(-1) {}

	node& getmin(int * D, int n)
	{
		int min=-1;
		for (int i=0;i<n;i++)		//znajdzie pierwszy niezerowy indeks
			if (D[i] >0)
			{
				min = i;
				break;
			}
		if (min==-1)
			return null;

		for (int i=min+1;i<this->count;i++)
			if (i!=min)
				if (D[i] >0)
					if (D[i] < D[min])
						min=i;

		return wezly[min];

	}

	void addNode(node& toAdd)
	{
		if (count==0)
		{
			count =1;
			wezly = new node[count];
			wezly[0] = toAdd;
			return;
		}
		else if (count >0)
		{
			++count;
			node * nowe = new node[count];
			for (int i=0;i<count-1;i++)
				nowe[i] = wezly[i];

			nowe[count-1] = toAdd;
			delete[] wezly;
			wezly = nowe;
			nowe=0;
		}
	}

	void delNode(node& toAdd)
		{
			if (count==0)
				return;

			else if (count >0)
			{
				int j=0;
				node * nowe = new node[count-1];
				for (int i=0;i<count;i++)
					if (wezly[i] != toAdd)
						nowe[j++] = wezly[i];

				delete[] wezly;
				wezly = nowe;
				nowe = 0;
			}
		}

	node& nodeById(int id)
	{
		for (int i=0;i<count;i++)
			if (wezly[i].nr == id)
				return wezly[i];

		return null;
	}

	bool empty()
	{
		return count;
	}


};

#endif /* CLASY_H_ */
